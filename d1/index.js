const http = require("http");
const port = 4000;

const server = http.createServer((request, response) => {
    // HTTP method can be accessed via "method" property inside the "request" object; they are to be written in ALL CAPS
    // GET REQUEST
    if (request.url === "/items" && request.method === "GET") 
    {
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Data retrieved from the database.");
    }

    // POST REQUEST
    // "POST" means adding/creating information
    if (request.url === "/items" && request.method === "POST")
    {
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Data to be sent to the database.");
    }
});

/* 
    GET method is one of the HTTPS methods that we will be using from this point.
        GET method means that we will be retrieving or reading information
*/

/* 
    POSTMAN
        - since the Node.js application that we are building is a backend application, and no frontend application yet to process requests, we will be using Postman to process the requests
*/

server.listen(port);
console.log(`Server now running at port: ${port}`);


// npx kill-port <port> - used to terminate the port activity