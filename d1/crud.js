const http = require("http");
const port = 4000;

// mock database
let directory = [
    {
        "name": "Brandom",
        "email": "brandon@mail.com"
    },
    {
        "name": "Jobert",
        "email": "jobert@mail.com"
    }
];

const server = http.createServer((request, response) => {
    
    // route for returning all items upon receiving a GET method request
        // Requests the "/users" path and "GET"s information
    if (request.url === "/users" && request.method === "GET") 
    {
        // sets the status code to 200, means OK
        // sets the response output to JSON data type
        response.writeHead(200, {"Content-Type": "application/json"});
        // response.write() - write any specified string
        // argument must be a type of string, otherwise it will return an error
        response.write(JSON.stringify(directory));
        // ends the response process
        response.end();
    }

    if (request.url === "/users" && request.method === "POST") 
    {
        let requestBody = "";
        /* 
            A stream is a sequence of data

            data is received from the client and is processed in the "data" stream
            when the information provided from the request object enters a sequence called "data" the code below will trigger
            - "data" step - this reads the "data" stream and processes it as the request body
        */
        // assigns the data retrieved from the data stream to requestBody
        request.on("data", data => requestBody += data);
        // request end step - only runs after the request has completely been sent
        request.on("end", function(){
            // checks if the requestBody is a STRING
            console.log(requestBody);
            console.log(typeof requestBody);

            // convert to JSON to access its properties
            requestBody = JSON.parse(requestBody);

            let newUser = {
                "name": requestBody.name,
                "email": requestBody.email
            }

            // Adds the new user into the mock database
            directory.push(newUser);
            console.log(directory);

            response.writeHead(200, {"Content-Type": "application/json"});
            response.write(JSON.stringify(newUser));
            response.end();
        });
    }
    
    
});

server.listen(port);
console.log(`Server now running at port: ${port}`);